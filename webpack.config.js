
const path = require('path');

module.exports = {
  mode: 'production',
  entry: {
    chart1: './src/chart1.js',
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'src'),
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'all',
        },
        // Additional cache groups
        echarts: {
          test: /[\\/]node_modules[\\/]echarts[\\/]/,
          name: 'echarts',
          chunks: 'all',
          priority: 10,
        },
      },
    },
  },
};

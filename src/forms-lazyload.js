// Create an intersection observer
const observer1 = new IntersectionObserver(
    (entries) => {
      console.log("forms:" +entries);
  
      // Check if the element is intersecting
     
       if(entries[0].isIntersecting){
        loadScriptForms();
        observer1.disconnect();
       }          
     
    
    }
  );
  
  
  // Get the element you want to observe
  const formContainer = document.querySelector("#contact-us");
  
  // Start observing the element
  observer1.observe(formContainer);


  function loadScriptForms(){
   
    var v2Script = document.createElement("script");
    v2Script.type = "text/javascript";
    v2Script.src = "//js.hsforms.net/forms/embed/v2.js";
    v2Script.charset = "utf-8";
    v2Script.setAttribute("data-rocket-status", "executed");
    
    v2Script.onload=()=>{
        console.log("v2 loaded");
        var script = document.createElement("script");
        script.setAttribute("defer", "");
        script.setAttribute("data-hubspot-rendered", "true");
        script.setAttribute("data-rocket-status", "executed");
        
        var scriptContent = document.createTextNode(`
          hbspt.forms.create({
            region: "na1",
            portalId: "24443183",
            formId: "68ef039c-1e46-4080-9bbc-cc9991885345",
            target: "#contact-us-form"
          });
        `);
        
        script.appendChild(scriptContent);
        document.body.appendChild(script);
    }
    document.body.appendChild(v2Script);

  }


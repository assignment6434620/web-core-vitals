const observer2 = new IntersectionObserver(
    (entries) => {
      console.log("footer:" +entries);
  
      // Check if the element is intersecting
     
       if(entries[0].isIntersecting){
        loadScriptTag();
        observer2.disconnect();
       }          
     
    
    }
  );
  
  
  // Get the element you want to observe
  const footer = document.querySelector(".footer-section");
  
  // Start observing the element
  observer2.observe(footer);


  function loadScriptTag(){
    console.log("tag loaded")
    var tag=document.createElement("script");
    var scriptContent = document.createTextNode(`
    (function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({ "gtm.start": new Date().getTime(), event: "gtm.js" });
        var f = d.getElementsByTagName(s)[0],
          j = d.createElement(s),
          dl = l != "dataLayer" ? "&l=" + l : "";
        j.async = true;
        j.src = "https://www.googletagmanager.com/gtm.js?id=" + i + dl;
        f.parentNode.insertBefore(j, f);
      })(window, document, "script", "dataLayer", "GTM-NT4XM5M");
  `);
    
  tag.appendChild(scriptContent);
  document.body.appendChild(tag);

  }

  




const observer3 = new IntersectionObserver(
    (entries) => {
      console.log("footer:" +entries);
  
      // Check if the element is intersecting
     
       if(entries[0].isIntersecting){
        loadScriptChat();
        observer3.disconnect();
       }          
     
    
    }
  );
  

    // Get the element you want to observe
    const chat = document.querySelector("#our-pricing");
  
    // Start observing the element
    observer3.observe(chat);
  

    function loadScriptChat(){
        console.log("chat loaded");
        const script = document.createElement('script');
        script.src = 'https://js.usemessages.com/conversations-embed.js';
        script.async = true;
        script.type = 'text/javascript';
        script.id = 'hubspot-messages-loader';
        script.setAttribute('data-loader', 'hs-scriptloader');
        script.setAttribute('data-hsjs-portal', '24443183');
        script.setAttribute('data-hsjs-env', 'prod');
        script.setAttribute('data-hsjs-hublet', 'na1');
        
        document.body.appendChild(script);
        
    }

